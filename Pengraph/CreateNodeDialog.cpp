#include "CreateNodeDialog.h"
#include "ui_CreateNodeDialog.h"

CreateNodeDialog::CreateNodeDialog(QWidget *parent) : QDialog(parent), ui(new Ui::CreateNodeDialog)
{
    ui->setupUi(this);
    ui->comboBox->addItem(QString::fromUtf8("Carré"), SQUARE);
    ui->comboBox->addItem(QString::fromUtf8("Cercle"), CIRCLE);
    ui->comboBox->addItem(QString::fromUtf8("Triangle"), TRIANGLE);
    ui->comboBox->addItem(QString::fromUtf8("Croix"), CROSS);

    ui->textEditColor->setText(QString::fromUtf8("#3296FA"));
}

CreateNodeDialog::~CreateNodeDialog()
{
    delete ui;
}

float CreateNodeDialog::getX()
{

}

float CreateNodeDialog::getY()
{

}

string CreateNodeDialog::getName()
{
    return ui->textEditName->toPlainText().toStdString();
}

int CreateNodeDialog::getWidth()
{

}

Shape CreateNodeDialog::getShape()
{
    return (Shape)ui->comboBox->currentIndex();
}

QColor CreateNodeDialog::getColor()
{
    return QColor(ui->textEditColor->toPlainText());
}

void CreateNodeDialog::setX(float x)
{

}

void CreateNodeDialog::setY(float y)
{

}

void CreateNodeDialog::setName(string s)
{
    ui->textEditName->setText(s.c_str());
}

void CreateNodeDialog::setWidth(int w)
{

}

void CreateNodeDialog::setShape(Shape shape)
{
    ui->comboBox->setCurrentIndex(shape);
}

void CreateNodeDialog::setColor(QColor color)
{
    ui->textEditColor->setText(color.name());
}
