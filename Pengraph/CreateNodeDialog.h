#ifndef CREATENODEDIALOG_H
#define CREATENODEDIALOG_H

#include <QDialog>
#include "Node.h"

namespace Ui
{
    class CreateNodeDialog;
}

class CreateNodeDialog : public QDialog
{
    Q_OBJECT
    
    public slots:

    public:
        explicit CreateNodeDialog(QWidget *parent = 0);
        ~CreateNodeDialog();
        float getX();
        float getY();
        string getName();
        int getWidth();
        Shape getShape();
        QColor getColor();
        void setX(float x);
        void setY(float y);
        void setName(string s);
        void setWidth(int w);
        void setShape(Shape shape);
        void setColor(QColor color);

    private:
        Ui::CreateNodeDialog *ui;
};

#endif // CREATENODEDIALOG_H
