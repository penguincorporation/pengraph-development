#include "Edge.h"

Edge::Edge()
{
    this->name = "";
    this->width = 1;
    this->color = QColor(0, 0, 0, 0);
    this->source = "";
    this->target = "";
}

Edge::Edge(string name, int width, QColor color, string source, string target)
{
    this->name = name;
    this->width = width;
    this->color = color;
    this->source = source;
    this->target = target;
}

Edge::~Edge()
{

}

void Edge::draw()
{

}

string Edge::getName()
{
    return this->name;
}

void Edge::setName(string name)
{
    this->name = name;
}

int Edge::getWidth()
{
    return this->width;
}

void Edge::setWidth(int width)
{
    this->width = width;
}

QColor Edge::getColor()
{
    return this->color;
}

void Edge::setColor(QColor color)
{
    this->color = color;
}

string Edge::getSource()
{
    return this->source;
}

void Edge::setSource(string source)
{
    this->source = source;
}

string Edge::getTarget()
{
    return this->target;
}

void Edge::setTarget(string target)
{
    this->target = target;
}
