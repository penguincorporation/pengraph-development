#ifndef EDGE_H
#define EDGE_H

#include <string>
#include <QColor>
#include "Node.h"

class Edge
{
    public:
        Edge();
        Edge(string name, int width, QColor color, string source, string target);
        ~Edge();
        void draw();
        string getName();
        void setName(string name);
        int getWidth();
        void setWidth(int width);
        QColor getColor();
        void setColor(QColor color);
        string getSource();
        void setSource(string source);
        string getTarget();
        void setTarget(string target);

    private:
        string name;
        int width;
        QColor color;
        string source;
        string target;
};

#endif // EDGE_H
