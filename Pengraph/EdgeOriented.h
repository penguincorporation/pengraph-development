#ifndef EDGEORIENTED_H
#define EDGEORIENTED_H

#include "Edge.h"

class EdgeOriented : public Edge
{
    public:
        EdgeOriented();
        ~EdgeOriented();
        void setDirectedToDestination(bool directedToDestination);

    private:
        bool directedToDestination;

};

#endif // EDGEORIENTED_H
