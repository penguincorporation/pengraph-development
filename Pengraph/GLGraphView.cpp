#include "GLGraphView.h"
#include <iostream>
using namespace std;

GLGraphView::GLGraphView(QWidget *parent) : QGLWidget(parent)
{
    position = QVector3D(0, 0, -10);
    rotation = QVector3D(0, 0, 0);
    scale = 4;
    installEventFilter(this);
    moving=false;
    selectedNode=nullptr;
    currentMode = DRAGGING;
    ctrlPressed = false;
    zoom = 1;
    dragging = false;
}

GLGraphView::~GLGraphView()
{
    delete graph;
}

void GLGraphView::setGraph(Graph *graph)
{
    this->graph = graph;
}

Graph *GLGraphView::getGraph()
{
    return this->graph;
}

QVector2D GLGraphView::raycast(int x, int y)
{
    QVector2D center = QVector2D(this->width() / 2, this->height() / 2);
    QVector2D relativeToCenter = QVector2D(x - center.x(), center.y() - y); //On inverse pour y, puisque les ordonnées OpenGL sont les opposées du sens classique.
    QVector2D relativeToSize = QVector2D(relativeToCenter.x() / this->width(), relativeToCenter.y() / this->height());
    QVector2D relativeToScale = QVector2D(relativeToSize.x() * scale*2, relativeToSize.y() * scale*2);
    return relativeToScale;

}

void GLGraphView::initializeGL()
{
    qglClearColor(Qt::black);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glShadeModel(GL_SMOOTH);
    /*
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    static GLfloat lightPosition[4] = { 0, 0, 10, 1.0 };
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    */
}

void GLGraphView::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(position.x(), position.y(), position.z());
    glRotatef(rotation.x() / 16.0, 1.0, 0.0, 0.0);
    glRotatef(rotation.y() / 16.0, 0.0, 1.0, 0.0);
    glRotatef(rotation.z() / 16.0, 0.0, 0.0, 1.0);
    draw();
}

void GLGraphView::resizeGL(int w, int h)
{
    glViewport(0, 0, w*zoom, h*zoom);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-scale, scale, -scale, scale, 1.0, 15.0);
    glMatrixMode(GL_MODELVIEW);
/*
    if (w<h)
    {
        glViewport(0,(h-w)/2,w,w);
    }
    else
    {
        glViewport((w-h)/2,0,h,h);
    }
*/
}

void GLGraphView::draw()
{
    drawNodes();
    drawEdges();
    if (dragging)
    {
        drawSelection();
    }
}

void GLGraphView::drawNodes()
{
    foreach(Node *n, graph->getNodes())
    {
        float x = n->getPos().x();
        float y = n->getPos().y();
        float w = n->getWidth();
        qglColor(n->getColor());
        if (draggingNodes.contains(n))
        {
            // Nouvelle couleur degueulasse pour montrer qu il est selectionne
            qglColor(QColor(0,250,0));
        }
        switch(n->getShape())
        {
            case SQUARE:
                glBegin(GL_QUADS);
                    glVertex2f(x - w / 2, y + w / 2);
                    glVertex2f(x - w / 2, y - w / 2);
                    glVertex2f(x + w / 2, y - w / 2);
                    glVertex2f(x + w / 2, y + w / 2);
                glEnd();
            break;

            case CIRCLE:
                glBegin(GL_LINE_LOOP);
                    for (int i=0; i < 360; i++)
                    {
                        float degInRad = i*DEG2RAD;
                        glVertex2f(cos(degInRad) * w + x,sin(degInRad) * w + y);
                    }
                glEnd();
            break;

            case TRIANGLE:
                glBegin(GL_TRIANGLES);
                    glVertex2f(x - w / 2, y - w / 2);
                    glVertex2f(x + w / 2, y - w / 2);
                    glVertex2f(x, y + w / 2);
                glEnd();
            break;

            case CROSS:
                glBegin(GL_LINES);
                    glVertex2f(x - w / 2, y + w / 2);
                    glVertex2f(x + w / 2, y - w / 2);
                glEnd();
                glBegin(GL_LINES);
                    glVertex2f(x - w / 2, y - w / 2);
                    glVertex2f(x + w / 2, y + w / 2);
                glEnd();
            break;

            default:
            break;
        }
    }
}

void GLGraphView::drawEdges()
{
    foreach(Edge *e, graph->getEdges())
    {
        QVector2D sourcePos = graph->getNodes()[e->getSource()]->getPos();
        QVector2D targetPos = graph->getNodes()[e->getTarget()]->getPos();
        qglColor(e->getColor());

        glBegin(GL_LINES);
            glVertex2f(sourcePos.x(), sourcePos.y());
            glVertex2f(targetPos.x(), targetPos.y());
        glEnd();
    }
}

void GLGraphView::drawSelection()
{
    qglColor(QColor(50,50,150));
    glBegin(GL_LINES);
        glVertex2f(draggingPos->x(), draggingPos->y());
        glVertex2f(draggingPos->x(), mousePos->y());
    glEnd();
    glBegin(GL_LINES);
        glVertex2f(draggingPos->x(), mousePos->y());
        glVertex2f(mousePos->x(), mousePos->y());
    glEnd();
    glBegin(GL_LINES);
        glVertex2f(mousePos->x(), mousePos->y());
        glVertex2f(mousePos->x(), draggingPos->y());
    glEnd();
    glBegin(GL_LINES);
        glVertex2f(mousePos->x(), draggingPos->y());
        glVertex2f(draggingPos->x(), draggingPos->y());
    glEnd();
}

bool GLGraphView::eventFilter(QObject *obj, QEvent *event)
{
    QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
    QVector2D picking = raycast(mouseEvent->x(),mouseEvent->y());
//TODO : Faire un boutton dans le design qui permet de passer du mode Noeud au mode Arête
        //Création d'un noeud, déplacement d'un noeud
    if (currentMode == NODEDRAWING)
    {
        //Clic gauche
        if (event->type() == QEvent::MouseButtonPress)
        {
            if (mouseEvent->button() == Qt::LeftButton)
            {
                Node *nodePicked = this->getGraph()->contains(picking.x(),picking.y());
                if (nodePicked==nullptr)
                {
                    CreateNodeDialog *cfdb = new CreateNodeDialog;
                    cfdb->setX(mouseEvent->x());
                    cfdb->setY(mouseEvent->y());

                    if(cfdb->exec() == QDialog::Accepted)
                    {
                        Node *newNode = new Node(raycast(mouseEvent->x(), mouseEvent->y()), cfdb->getShape(), cfdb->getName(), 1.0, cfdb->getColor());
                        graph->createNode(this->generateNodeID(), newNode);
                    }
                }
            }
        }
    }
    // Création d'arrete
    else if (currentMode == EDGEDRAWING)
    {
        //Clic gauche
        if (event->type() == QEvent::MouseButtonPress)
        {
            if (mouseEvent->button() == Qt::LeftButton)
            {
                Node *nodePicked = this->getGraph()->contains(picking.x(),picking.y());
                //Si on a selectionné un noeud, alors on commence la création d'une arête
                if (nodePicked!=nullptr)
                {
                    drawingNode= new Node(picking.x(),picking.y(),Shape::SQUARE,"",1.0,QColor(50,50,50,50));
                    selectedNode = nodePicked;
                    string IDTMP = this->generateNodeID();
                    graph->setIDINTERDIT(IDTMP);
                    graph->createNode(IDTMP,drawingNode);
                    drawingEdge = new Edge("coucou",1,QColor(50,150,250),graph->getNodeID(selectedNode),graph->getNodeID(drawingNode));
                    graph->createEdge(this->generateEdgeID(),drawingEdge);

                }
            }
        }
        //Déplacement de la souris lorsque l'on est entrain de dessiner une arête.
        if (event->type() == QEvent::MouseMove && drawingEdge)
        {
            drawingNode->setPos(picking.x(),picking.y());
        }
        //au relachement de la souris, on dessine l'arête.
        if (event->type() == QEvent::MouseButtonRelease && drawingEdge)
        {
            Node *nodePicked = this->getGraph()->contains(picking.x(),picking.y());
            //Si il y a un noeud à l'endroit relaché, alors on fait une arête entre le noeud de base et ce noeud.
            if (nodePicked != nullptr && nodePicked!= drawingNode)
            {
                drawingEdge->setTarget(graph->getNodeID(nodePicked));
                graph->deleteNode(graph->getNodeID(drawingNode));
                delete drawingNode;
            }
            //Sinon, on créer un nouveau noeud et on dessine une arête entre le noeud de base et le nouveau.
            else
            {
                CreateNodeDialog *cfdb = new CreateNodeDialog;
                if(cfdb->exec() == QDialog::Accepted)
                {
                    drawingNode->setShape(cfdb->getShape());
                    drawingNode->setName(cfdb->getName());
                    //drawingNode->setWidth(cfdb->getWidth());
                    drawingNode->setColor(cfdb->getColor());
                }
                drawingNode=nullptr;
                graph->setIDINTERDIT("");
            }
        }
    }
    else if (currentMode == DRAGGING)
    {
        //Clic
        if (event->type() == QEvent::MouseButtonPress)
        {
            if (mouseEvent->button() == Qt::LeftButton)
            {
                Node *nodePicked = this->getGraph()->contains(picking.x(),picking.y());
                if (nodePicked!=nullptr)
                {
                    selectedNode = nodePicked;
                }
                //Si on selectionne un seul noeud
                // On commence le dessin du rectangle de selection
                else
                {
                    dragging=true;
                    draggingPos = new QVector2D(picking.x(),picking.y());
                    mousePos = new QVector2D(picking.x(),picking.y());
                }
            }
        }
        //Fin du clic
        if (event->type() == QEvent::MouseButtonRelease)
        {
            if (mouseEvent->button() == Qt::LeftButton)
            {
                // On arrete le dessin du rectangle de selection
                if (selectedNode!=nullptr)
                {
                    selectedNode = nullptr;
                }
                else
                {
                    dragging=false;
                }
            }
        }
        //Deplacement de la souris
        if (event->type() == QEvent::MouseMove)
        {
            if (selectedNode!=nullptr)
            {
                if (draggingNodes.isEmpty())
                {
                    selectedNode->setPos(picking.x(),picking.y());
                }
                else
                {
                    float translX = picking.x() - selectedNode->getPos().x();
                    float translY = picking.y() - selectedNode->getPos().y();
                    foreach(Node *n, draggingNodes)
                    {
                        n->setPos(n->getPos().x()+translX,n->getPos().y()+translY);
                    }
                }
            }
            else if (dragging)
            {
                mousePos->setX(picking.x());
                mousePos->setY(picking.y());
                // On verifie quels noeuds sont selectionne
                foreach(Node *n, graph->getNodes())
                {
                        float x = n->getPos().x();
                        float y = n->getPos().y();
                        float w = n->getWidth();
                        if (x-w/2 > draggingPos->x() && y-w/2 > draggingPos->y() && x+w/2 < mousePos->x() && y+w/2 < mousePos->y())
                        {
                            if (!draggingNodes.contains(n))
                            draggingNodes.append(n);
                        }
                        else if (draggingNodes.contains(n))
                        {
                            draggingNodes.remove(draggingNodes.indexOf(n));
                        }
                }
            }
        }
    }
    if (ctrlPressed)
    {
        if (event->type() == QEvent::Wheel)
        {
            QWheelEvent * wheelevent = static_cast<QWheelEvent*>(event);
            if (wheelevent->delta()>0)
            {
            zoom+=0.25;
            resizeGL(this->width(),this->height());
            }
            if (wheelevent->delta()<0)
            {
            zoom-=0.25;
            resizeGL(this->width(),this->height());
            }
        }
    }
    if(event->type() == QEvent::ContextMenu)
    {
        Node *nodePicked = this->getGraph()->contains(picking.x(),picking.y());
        QMenu *menu = new QMenu(this);
        //Menu Contextuel lorsque l'on selectionne un noeud
        if (!draggingNodes.empty())
        {
            selectedNode=nodePicked;
            // Action de modification
            QAction * modifier = new QAction("Modifier",this);
            menu->addAction(modifier);
            connect(modifier,SIGNAL(triggered()),this,SLOT(modifyingSelectedNodes()));
            // Action de copie
            /*
            QAction * copier = new QAction("Copier",this);
            menu->addAction(copier);
            connect(copier,SIGNAL(triggered()),this,SLOT(copyNode()));
            */
            // Action de découpage
            /*
            QAction * couper = new QAction("Couper",this);
            menu->addAction(couper);
            connect(couper,SIGNAL(triggered()),this,SLOT(cutNode()));
            */
            // Action de suppression
            QAction * supprimer = new QAction("Supprimer",this);
            menu->addAction(supprimer);
            connect(supprimer,SIGNAL(triggered()),this,SLOT(deletingSelectedNodes()));
        }
        else if (nodePicked!=nullptr)
        {
            selectedNode=nodePicked;
            // Action de modification
            QAction * modifier = new QAction("Modifier",this);
            menu->addAction(modifier);
            connect(modifier,SIGNAL(triggered()),this,SLOT(contextModifyNode()));
            // Action de copie
            /*
            QAction * copier = new QAction("Copier",this);
            menu->addAction(copier);
            connect(copier,SIGNAL(triggered()),this,SLOT(copyNode()));
            */
            // Action de découpage
            /*
            QAction * couper = new QAction("Couper",this);
            menu->addAction(couper);
            connect(couper,SIGNAL(triggered()),this,SLOT(cutNode()));
            */
            // Action de suppression
            QAction * supprimer = new QAction("Supprimer",this);
            menu->addAction(supprimer);
            connect(supprimer,SIGNAL(triggered()),this,SLOT(contextDeleteNode()));
        }
        menu->exec(mouseEvent->globalPos());
    }

    updateGL();
    return false;
}

void GLGraphView::setCTRL(bool ctrl)
{
    this->ctrlPressed = ctrl;
}
bool GLGraphView::getCTRL()
{
    return this->ctrlPressed;
}

void GLGraphView::setMode(Mode newMode)
{
    this->currentMode = newMode;
}
void GLGraphView::deletingSelectedNodes()
{
    for (Node * n : draggingNodes)
    {
        foreach(Edge * e , graph->getEdges())
        {
            if (e->getSource() == graph->getNodeID(n) || e->getTarget() == graph->getNodeID(n))
            {
                graph->deleteEdge(graph->getEdgeID(e));
                delete e;
            }
        }
        graph->deleteNode(graph->getNodeID(n));
        delete n;
    }
    updateGL();
}
void GLGraphView::modifyingSelectedNodes()
{
    CreateNodeDialog *cfdb = new CreateNodeDialog;
    if(cfdb->exec() == QDialog::Accepted)
    {
        foreach(Node * n, draggingNodes)
        {
            n->setShape(cfdb->getShape());
            n->setName(cfdb->getName());
            //drawingNode->setWidth(cfdb->getWidth());
            n->setColor(cfdb->getColor());
        }
    }
}
void GLGraphView::contextDeleteNode()
{
    foreach(Edge * e , graph->getEdges())
    {
        if (e->getSource() == graph->getNodeID(selectedNode) || e->getTarget() == graph->getNodeID(selectedNode))
        {
            graph->deleteEdge(graph->getEdgeID(e));
            delete e;
        }
    }
    graph->deleteNode(graph->getNodeID(selectedNode));
    delete selectedNode;
}

void GLGraphView::contextModifyNode()
{
    CreateNodeDialog *cfdb = new CreateNodeDialog;
    if(cfdb->exec() == QDialog::Accepted)
    {
        selectedNode->setShape(cfdb->getShape());
        selectedNode->setName(cfdb->getName());
        //drawingNode->setWidth(cfdb->getWidth());
        selectedNode->setColor(cfdb->getColor());
    }
}
void GLGraphView::copy()
{
    foreach (Node * n, draggingNodes)
    {
        Node * nodetmp = new Node(n->getPos().x(),n->getPos().y(),n->getShape(),n->getName(),n->getWidth(),n->getColor());
        string tmp = this->generateNodeID();
        qDebug(tmp.c_str());
        //nodesBuffer.insert(this->generateNodeID(),nodetmp);
        nodesBuffer.insert(tmp,nodetmp);
    }

    foreach (Edge * e, graph->getEdges())
    {
        int cpt=0;
        foreach(Node * n, draggingNodes)
        {
            if (e->getSource()==graph->getNodeID(n) || e->getTarget()==graph->getNodeID(n))
            {
                qDebug("Yeah");
                cpt++;
            }
        }
        if (cpt>=2)
        {
            qDebug("One Edge");
            string sourceTMP;
            string targetTMP;
            float x = graph->getNode(e->getSource())->getPos().x();
            float y = graph->getNode(e->getSource())->getPos().y();
            foreach(Node *n, nodesBuffer)
            {
                //qDebug("Coord Noeud :  :(%f,%f)",n->getPos().x(),n->getPos().y());
                if (x > n->getPos().x()-n->getWidth() && x < n->getPos().x() + n->getWidth() && y > n->getPos().y()-n->getWidth() && y < n->getPos().y() + n->getWidth())
                {
                    sourceTMP = nodesBuffer.key(n);
                }
            }
            x = graph->getNode(e->getTarget())->getPos().x();
            y = graph->getNode(e->getTarget())->getPos().y();
            foreach(Node *n, nodesBuffer)
            {
                //qDebug("Coord Noeud :  :(%f,%f)",n->getPos().x(),n->getPos().y());
                if (x > n->getPos().x()-n->getWidth() && x < n->getPos().x() + n->getWidth() && y > n->getPos().y()-n->getWidth() && y < n->getPos().y() + n->getWidth())
                {
                    targetTMP = nodesBuffer.key(n);
                }
            }
            qDebug((sourceTMP + " " + targetTMP).c_str());
            Edge * edgetmp = new Edge(e->getName(),e->getWidth(),e->getColor(),sourceTMP,targetTMP);
            edgesBuffer.insert(this->generateEdgeID(),edgetmp);
        }
    }

}
void GLGraphView::cut()
{
    this->copy();
    this->deletingSelectedNodes();
}
void GLGraphView::paste()
{
    foreach (Node * n, nodesBuffer)
    {
        graph->createNode(nodesBuffer.key(n),n);
    }
    foreach (Edge * e, edgesBuffer)
    {
        graph->createEdge(edgesBuffer.key(e),e);
    }
    updateGL();
}

string GLGraphView::generateNodeID()
{
    int actualID = graph->getCurrentNodeID()+1;
    if (!nodesBuffer.empty())
    {
        QMapIterator<string, Node*> i(nodesBuffer);
        while (i.hasNext()) {
            i.next();
            if (i.key()==QString::number(actualID).toStdString())
            {
                actualID++;
            }
        }
    }
    graph->setCurrentNodeID(actualID);
    return QString::number(actualID).toStdString();
}
string GLGraphView::generateEdgeID()
{
    int actualID = graph->getCurrentEdgeID()+1;
    if (!edgesBuffer.empty())
    {
        QMapIterator<string, Edge*> i(edgesBuffer);
        while (i.hasNext()) {
            i.next();
            if (i.key()==QString::number(actualID).toStdString())
            {
                actualID++;
            }
        }
    }
    graph->setCurrentEdgeID(actualID);
    return QString::number(actualID).toStdString();
}
