#ifndef GLGRAPHVIEW_H
#define GLGRAPHVIEW_H

#include <QGLWidget>
#include <QVector3D>
#include <QEvent>
#include <QMouseEvent>
#include <QDialog>
#include <QMenu>
#include "Graph.h"
#include "CreateNodeDialog.h"
#include <QVector>
const float DEG2RAD = 3.14159/180;

enum Mode { NODEDRAWING, EDGEDRAWING, DRAGGING};

class GLGraphView : public QGLWidget
{
    Q_OBJECT

    public:
        explicit GLGraphView(QWidget *parent = 0);
        ~GLGraphView();
        void setGraph(Graph *graph);
        Graph *getGraph();
        void setCTRL(bool ctrl);
        bool getCTRL();
        void setMode(Mode newMode);
        void copy();
        void cut();
        void paste();
        string generateNodeID();
        string generateEdgeID();
    signals:

    public slots:
        void contextDeleteNode();
        void contextModifyNode();
        void deletingSelectedNodes();
        void modifyingSelectedNodes();
    protected:
        void initializeGL();
        void paintGL();
        void resizeGL(int w, int h);

        void drawNodes();
        void drawEdges();
        void drawSelection();
        bool eventFilter(QObject *obj, QEvent *event);

    private:
        QVector2D raycast(int x, int y);
        void draw();
        bool ctrlPressed;
        float zoom;
        QVector3D position;
        QVector3D rotation;
        float scale;
        Node * drawingNode;
        Node * selectedNode;
        Edge * drawingEdge;
        bool moving;
        Graph *graph;
        Mode currentMode;
        bool dragging;
        QVector2D * draggingPos;
        QVector2D * mousePos;
        QVector<Node *> draggingNodes;
        QMap<string, Node *> nodesBuffer;
        QMap<string, Edge *> edgesBuffer;
};

#endif // GLGRAPHVIEW_H
