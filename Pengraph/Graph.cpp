#include "Graph.h"

Graph::Graph()
{
    this->name = "";
    this->file = "";
    this->defaultEdgeColor = QColor(0, 0, 0, 0);
    this->defaultWidth = 1;
}

Graph::Graph(const Graph &graph)
{
    this->name = graph.name;
    this->file = graph.file;
    this->defaultEdgeColor = graph.defaultEdgeColor;
    this->defaultWidth = graph.defaultWidth;
    this->currentNodeID = 0;
    this->currentEdgeID = 0;
}

Graph::~Graph()
{
    foreach(Edge *e, edges)
    {
        delete e;
    }

    foreach(Node *n, nodes)
    {
        delete n;
    }
}
void Graph::setCurrentNodeID(int newID)
{
    this->currentNodeID=newID;
}
void Graph::setCurrentEdgeID(int newID)
{
    this->currentEdgeID=newID;
}

int Graph::getCurrentNodeID()
{
    return currentNodeID;
    /*
    currentNodeID++;
    return QString::number(currentNodeID-1).toStdString();
    */
}
int Graph::getCurrentEdgeID()
{
        return currentEdgeID;
        /*
    currentEdgeID++;
    return QString::number(currentEdgeID-1).toStdString();
    */
}
QColor Graph::getDefaultEdgeColor()
{
    return defaultEdgeColor;
}

void Graph::setDefaultEdgeColor(QColor color)
{
    defaultEdgeColor = color;
}

QMap<string, Node *> Graph::getNodes()
{
    return this->nodes;
}

QMap<string, Edge*> Graph::getEdges()
{
    return this->edges;
}

void Graph::select()
{

}
void Graph::setIDINTERDIT(string id)
{
    IDINTERDIT=id;
}

void Graph::createNode(string id, Node *node)
{
    nodes.insert(id, node);
}

void Graph::moveNode()
{

}

void Graph::deleteNode(string id)
{
    nodes.remove(id);
}

void Graph::createEdge(string id, Edge *edge)
{
    edges.insert(id, edge);
}

void Graph::deleteEdge(string id)
{
    edges.remove(id);
}

void Graph::applyAlgorithm()
{

}
string Graph::getNodeID(Node * noeud)
{
    //TODO : Retourner l'id selon le noeud.
    QMapIterator<string, Node*> i(nodes);
    while (i.hasNext()) {
        i.next();
        if (i.value()==noeud)
        {
            return i.key();
        }

    }
    return "";
}

string Graph::getEdgeID(Edge * noeud)
{
    //TODO : Retourner l'id selon le noeud.
    QMapIterator<string, Edge*> i(edges);
    while (i.hasNext()) {
        i.next();
        if (i.value()==noeud)
        {
            return i.key();
        }

    }
    return "";
}

Node * Graph::contains(float x, float y)
{
    //qDebug("Coord Souris :(%f,%f)",x,y);
    foreach(Node *n, nodes)
    {
        //qDebug("Coord Noeud :  :(%f,%f)",n->getPos().x(),n->getPos().y());
        if (nodes.key(n)!=IDINTERDIT)
        {
        if (x > n->getPos().x()-n->getWidth() && x < n->getPos().x() + n->getWidth() && y > n->getPos().y()-n->getWidth() && y < n->getPos().y() + n->getWidth())
        {
            return n;
        }
        }
    }
    return nullptr;
}

Node * Graph::getNode(string id)
{
    return nodes[id];
}
Edge * Graph::getEdge(string id)
{
    return edges[id];
}
