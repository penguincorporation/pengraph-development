#ifndef GRAPH_H
#define GRAPH_H

#include <string>
#include <QMap>
#include <QMapIterator>
#include <QColor>
#include "Node.h"
#include "Edge.h"

class Graph
{
    public:
        explicit Graph();
        explicit Graph(const Graph &graph);
        ~Graph();
        QColor getDefaultEdgeColor();
        void setDefaultEdgeColor(QColor color);
        QMap<string, Node*> getNodes();
        QMap<string, Edge*> getEdges();
        Node * getNode(string id);
        Edge * getEdge(string id);
        void select();
        void createNode(string id, Node *node);
        void moveNode();
        void deleteNode(string id);
        void createEdge(string id, Edge *edge);
        void deleteEdge(string id);
        void applyAlgorithm();
        Node * contains(float x, float y);
        string getNodeID(Node *noeud);
        string getEdgeID(Edge *noeud);
        int getCurrentNodeID();
        int getCurrentEdgeID();
        void setCurrentNodeID(int newID);
        void setCurrentEdgeID(int newID);
        void setIDINTERDIT(string id);
    private:
        string name;
        string file;
        QColor defaultEdgeColor;
        int defaultWidth;
        int currentNodeID;
        int currentEdgeID;
        QMap<string, Node*> nodes;
        QMap<string, Edge*> edges;
        string IDINTERDIT;
};

#endif // GRAPH_H
