#include "MainWindow.h"
#include "ui_mainwindow.h"
#include <QPushButton>
#include <QListView>
#include <iostream>
using namespace std;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    view = ui->glGraphView;
    newGraph();
    view->setGraph(graph);
    this->installEventFilter(this);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete view;
}

Graph *MainWindow::getGraph()
{
    return this->graph;
}
GLGraphView *MainWindow::getGraphView()
{
return this->view;
}

void MainWindow::setGraph(Graph *graph)
{
    this->graph = graph;
}

void MainWindow::newGraph()
{
    graph = new Graph();
}

void MainWindow::openGraph()
{
    QFileInfo file(QFileDialog::getOpenFileName(this, tr("Ouvrir un graphe"), "~", tr("Fichiers de graphe (*.graphml *.dot)")));
    QFile f(file.absoluteFilePath());
    if(file.completeSuffix() == "graphml")
    {
        QDomDocument xmlBOM;
        xmlBOM.setContent(&f);

        QDomElement root = xmlBOM.documentElement();
        parseGraphXML(root);
        f.close();
    }
    else if(file.completeSuffix() == "dot")
    {

    }
    view->updateGL();
}

void MainWindow::parseGraphXML(QDomElement element)
{
    while(!element.isNull())
    {
        if (element.tagName() == "graphml" || element.tagName() == "graph")
        {
            parseGraphXML(element.firstChild().toElement());
        }
        else if (element.tagName() == "node")
        {
            string id = element.attribute("id").toStdString();
            int x = element.attribute("x").toInt();
            int y = element.attribute("y").toInt();
            Shape shape = static_cast<Shape>(element.attribute("shape").toInt());
            string name = element.attribute("name").toStdString();
            float width = element.attribute("width").toFloat();
            QColor color = QColor(element.attribute("color"));
            graph->createNode(id, new Node(x, y, shape, name, width, color));
        }
        else if (element.tagName() == "edge")
        {
            string id = element.attribute("id").toStdString();
            string name = element.attribute("name").toStdString();
            float width = element.attribute("width").toFloat();
            string source = element.attribute("source").toStdString();
            string target = element.attribute("target").toStdString();
            QColor color = QColor(element.attribute("color"));
            graph->createEdge(id, new Edge(name, width, color, source, target));
        }
        else
        {
            return;
        }
        element = element.nextSibling().toElement();
    }
}


bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
   // QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);

    if (event->type() == QKeyEvent::KeyPress)
    {
        QKeyEvent *ke = (QKeyEvent *) event;

        int keyValue = ke->key();
        pressedKeys.insert(keyValue);
        //qDebug(QString::number(keyValue).toStdString().c_str());
        // Touche controle
        if(keyValue == 16777249)
        {
            view->setCTRL(true);
        }
        // Touche suppr
        /*
        if(keyValue == 16777223)
        {
            view->deletingSelectedNodes();
        }
        */
        foreach(int i, pressedKeys)
        {
            qDebug(QString::number(i).toStdString().c_str());
        }

        if (pressedKeys.contains(67) && pressedKeys.contains(16777249))
        {
             qDebug("copy");
             view->copy();
        }
        if (pressedKeys.contains(85) && pressedKeys.contains(16777249))
        {
            qDebug("cut");
            view->cut();
        }
        if (pressedKeys.contains(83) && pressedKeys.contains(16777249))
        {
             qDebug("paste");
             view->paste();
        }
        // Changement de mode temporaire
        if (keyValue == 78)
        {
            view->setMode(NODEDRAWING);
        }
        if (keyValue == 69)
        {
            view->setMode(EDGEDRAWING);
        }
        if (keyValue == 68)
        {
            view->setMode(DRAGGING);
        }
    }
    if (event->type() == QKeyEvent::KeyRelease)
    {
        QKeyEvent *ke = (QKeyEvent *) event;
        int keyValue = ke->key();
        if (keyValue == 67 && pressedKeys.contains(16777249))
        {
             qDebug("copy");
             view->copy();
        }
        if (keyValue == 88 && pressedKeys.contains(16777249))
        {
            qDebug("cut");
            view->cut();
        }
        if (keyValue == 86 && pressedKeys.contains(16777249))
        {
             qDebug("paste");
             view->paste();
        }
        pressedKeys.remove(keyValue);
        if(keyValue == 16777249)
        {
            view->setCTRL(false);
        }
    }
    //updateGL();
    return false;
}
