#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "Graph.h"
#include <QMainWindow>
#include <QFileDialog>
#include <QFileInfo>
#include <QSet>
#include <QtXml>
#include "GLGraphView.h"

class MyQGLWidget;

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();
        Graph *getGraph();
        void setGraph(Graph *graph);
        GLGraphView *getGraphView();
    signals:
        void onOpenedGraph();

    public slots:
        void newGraph();
        void openGraph();
        void parseGraphXML(QDomElement element);

    private:
        Ui::MainWindow *ui;
        GLGraphView *view;
        Graph *graph;
        bool eventFilter(QObject *obj, QEvent *event);
        QSet<int> pressedKeys;
};

#endif // MAINWINDOW_H
