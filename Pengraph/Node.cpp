#include "Node.h"

Node::Node()
{
    this->pos = QVector2D(0, 0);
    this->shape = SQUARE;
    this->name = "";
    this->width = 1;
    this->color = QColor(0, 0, 0, 0);
}

Node::Node(float x, float y, Shape shape, string name, float width, QColor color)
{
    this->pos = QVector2D(x, y);
    this->shape = shape;
    this->name = name;
    this->width = width;
    this->color = color;
}

Node::Node(QVector2D pos, Shape shape, string name, float width, QColor color)
{
    this->pos = pos;
    this->shape = shape;
    this->name = name;
    this->width = width;
    this->color = color;
}

Node::~Node()
{

}

void Node::draw()
{

}

void Node::setPos(float x, float y)
{
    this->pos = QVector2D(x, y);
}

QVector2D Node::getPos()
{
    return this->pos;
}

void Node::setShape(Shape shape)
{
    this->shape = shape;
}

Shape Node::getShape()
{
    return this->shape;
}

void Node::setName(string name)
{
    this->name = name;
}

string Node::getName()
{
    return this->name;
}

void Node::setWidth(float width)
{
    this->width = width;
}

float Node::getWidth()
{
    return this->width;
}

void Node::setColor(QColor color)
{
    this->color = color;
}

QColor Node::getColor()
{
    return this->color;
}
