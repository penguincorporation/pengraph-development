#ifndef NODE_H
#define NODE_H

#include <string>
#include <QVector2D>
#include <QColor>
using namespace std;

enum Shape { SQUARE, CIRCLE, TRIANGLE, CROSS };

class Node
{
    public:
        Node();
        Node(float x, float y, Shape shape, string name, float width, QColor color);
        Node(QVector2D pos, Shape shape, string name, float width, QColor color);
        ~Node();
        void draw();
        void setPos(float x, float y);
        QVector2D getPos();
        void setShape(Shape shape);
        Shape getShape();
        void setName(string name);
        string getName();
        void setWidth(float width);
        float getWidth();
        void setColor(QColor color);
        QColor getColor();

    private:
        QVector2D pos;
        Shape shape;
        string name;
        float width;
        QColor color;
};

#endif // NODE_H
