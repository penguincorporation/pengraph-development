#-------------------------------------------------
#
# Project created by QtCreator 2015-01-26T16:37:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++11

TARGET      = Pengraph
TEMPLATE    = app
QT          += opengl xml

SOURCES += \
    MainWindow.cpp \
    Main.cpp \
    Graph.cpp \
    Node.cpp \
    Edge.cpp \
    EdgeOriented.cpp \
    GLGraphView.cpp \
    CreateNodeDialog.cpp

HEADERS  += \
    Graph.h \
    MainWindow.h \
    Node.h \
    Edge.h \
    EdgeOriented.h \
    GLGraphView.h \
    CreateNodeDialog.h

FORMS    += \
    mainwindow.ui \
    CreateNodeDialog.ui
