#-------------------------------------------------
#
# Project created by QtCreator 2015-03-02T09:00:04
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = mainTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += mainTest.cpp
