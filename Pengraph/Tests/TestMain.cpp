#include <QString>
#include <QtTest/QTest>

class TUTest : public QObject
{
    Q_OBJECT
    
public:
    TUTest();
    
private Q_SLOTS:
    void testCase1();
    //
    void toUpper();
    //
};

TUTest::TUTest()
{
}

void TUTest::testCase1()
{
    QVERIFY2(true, "Failure");
}
//
void TUTest::toUpper()
{
    QString str = "Hello";
    QCOMPARE(str.toUpper(),QString("HELLO"));
}
//

//QTEST_APPLESS_MAIN(TUTest)
QTEST_MAIN(TUTest)


#include "mainTest.moc"
