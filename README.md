#Pengraph

Pengraph est un éditeur de graphes, écrit en C++ avec Qt/OpenGL.

###I - Cloner

Afin de cloner le dépôt, utilisez la commande :

>\> git clone https://bitbucket.org/penguincorporation/pengraph-development.git Pengraph\ Development

N'oubliez pas d'ajouter tous les fichiers modifiés avec la commande :

>\> git add [YOUR FILES]

Puis, pour commit et push sur le server :

>\> git commit -m "MESSAGE" && git push -u origin master

###II - Introduction

###III - How to use