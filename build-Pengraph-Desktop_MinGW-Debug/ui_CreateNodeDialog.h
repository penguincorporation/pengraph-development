/********************************************************************************
** Form generated from reading UI file 'CreateNodeDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CREATENODEDIALOG_H
#define UI_CREATENODEDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QTextEdit>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_CreateNodeDialog
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayoutName;
    QLabel *labelName;
    QTextEdit *textEditName;
    QHBoxLayout *horizontalLayoutPos;
    QVBoxLayout *verticalLayoutPos;
    QLabel *labelPos;
    QHBoxLayout *horizontalLayoutPosValues;
    QSpacerItem *horizontalSpacer_3;
    QLabel *labelX;
    QSpinBox *spinBoxX;
    QLabel *labelY;
    QSpinBox *spinBoxY;
    QSpacerItem *horizontalSpacer_4;
    QVBoxLayout *verticalLayoutSize;
    QLabel *labelSize;
    QHBoxLayout *horizontalLayoutSize;
    QSpacerItem *horizontalSpacer;
    QSpinBox *spinBoxWidth;
    QSlider *horizontalSlider;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayoutShapeColor;
    QLabel *labelShape;
    QComboBox *comboBox;
    QLabel *labelColor;
    QTextEdit *textEditColor;
    QDialogButtonBox *buttonBoxValidation;

    void setupUi(QDialog *CreateNodeDialog)
    {
        if (CreateNodeDialog->objectName().isEmpty())
            CreateNodeDialog->setObjectName(QString::fromUtf8("CreateNodeDialog"));
        CreateNodeDialog->resize(282, 213);
        verticalLayout = new QVBoxLayout(CreateNodeDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayoutName = new QHBoxLayout();
        horizontalLayoutName->setObjectName(QString::fromUtf8("horizontalLayoutName"));
        horizontalLayoutName->setSizeConstraint(QLayout::SetNoConstraint);
        labelName = new QLabel(CreateNodeDialog);
        labelName->setObjectName(QString::fromUtf8("labelName"));

        horizontalLayoutName->addWidget(labelName);

        textEditName = new QTextEdit(CreateNodeDialog);
        textEditName->setObjectName(QString::fromUtf8("textEditName"));
        textEditName->setMaximumSize(QSize(16777215, 24));
        textEditName->setBaseSize(QSize(0, 0));
        textEditName->setFrameShape(QFrame::StyledPanel);
        textEditName->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        textEditName->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        horizontalLayoutName->addWidget(textEditName);


        verticalLayout->addLayout(horizontalLayoutName);

        horizontalLayoutPos = new QHBoxLayout();
        horizontalLayoutPos->setObjectName(QString::fromUtf8("horizontalLayoutPos"));
        verticalLayoutPos = new QVBoxLayout();
        verticalLayoutPos->setObjectName(QString::fromUtf8("verticalLayoutPos"));
        labelPos = new QLabel(CreateNodeDialog);
        labelPos->setObjectName(QString::fromUtf8("labelPos"));

        verticalLayoutPos->addWidget(labelPos);

        horizontalLayoutPosValues = new QHBoxLayout();
        horizontalLayoutPosValues->setObjectName(QString::fromUtf8("horizontalLayoutPosValues"));
        horizontalSpacer_3 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayoutPosValues->addItem(horizontalSpacer_3);

        labelX = new QLabel(CreateNodeDialog);
        labelX->setObjectName(QString::fromUtf8("labelX"));
        labelX->setMaximumSize(QSize(16, 16777215));

        horizontalLayoutPosValues->addWidget(labelX);

        spinBoxX = new QSpinBox(CreateNodeDialog);
        spinBoxX->setObjectName(QString::fromUtf8("spinBoxX"));

        horizontalLayoutPosValues->addWidget(spinBoxX);

        labelY = new QLabel(CreateNodeDialog);
        labelY->setObjectName(QString::fromUtf8("labelY"));
        labelY->setMaximumSize(QSize(16, 16777215));

        horizontalLayoutPosValues->addWidget(labelY);

        spinBoxY = new QSpinBox(CreateNodeDialog);
        spinBoxY->setObjectName(QString::fromUtf8("spinBoxY"));

        horizontalLayoutPosValues->addWidget(spinBoxY);

        horizontalSpacer_4 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayoutPosValues->addItem(horizontalSpacer_4);


        verticalLayoutPos->addLayout(horizontalLayoutPosValues);


        horizontalLayoutPos->addLayout(verticalLayoutPos);


        verticalLayout->addLayout(horizontalLayoutPos);

        verticalLayoutSize = new QVBoxLayout();
        verticalLayoutSize->setObjectName(QString::fromUtf8("verticalLayoutSize"));
        labelSize = new QLabel(CreateNodeDialog);
        labelSize->setObjectName(QString::fromUtf8("labelSize"));

        verticalLayoutSize->addWidget(labelSize);

        horizontalLayoutSize = new QHBoxLayout();
        horizontalLayoutSize->setObjectName(QString::fromUtf8("horizontalLayoutSize"));
        horizontalSpacer = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayoutSize->addItem(horizontalSpacer);

        spinBoxWidth = new QSpinBox(CreateNodeDialog);
        spinBoxWidth->setObjectName(QString::fromUtf8("spinBoxWidth"));
        spinBoxWidth->setReadOnly(true);

        horizontalLayoutSize->addWidget(spinBoxWidth);

        horizontalSlider = new QSlider(CreateNodeDialog);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setMaximumSize(QSize(100, 16777215));
        horizontalSlider->setOrientation(Qt::Horizontal);

        horizontalLayoutSize->addWidget(horizontalSlider);

        horizontalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayoutSize->addItem(horizontalSpacer_2);


        verticalLayoutSize->addLayout(horizontalLayoutSize);


        verticalLayout->addLayout(verticalLayoutSize);

        horizontalLayoutShapeColor = new QHBoxLayout();
        horizontalLayoutShapeColor->setObjectName(QString::fromUtf8("horizontalLayoutShapeColor"));
        labelShape = new QLabel(CreateNodeDialog);
        labelShape->setObjectName(QString::fromUtf8("labelShape"));

        horizontalLayoutShapeColor->addWidget(labelShape);

        comboBox = new QComboBox(CreateNodeDialog);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setModelColumn(0);

        horizontalLayoutShapeColor->addWidget(comboBox);

        labelColor = new QLabel(CreateNodeDialog);
        labelColor->setObjectName(QString::fromUtf8("labelColor"));

        horizontalLayoutShapeColor->addWidget(labelColor);

        textEditColor = new QTextEdit(CreateNodeDialog);
        textEditColor->setObjectName(QString::fromUtf8("textEditColor"));
        textEditColor->setMaximumSize(QSize(16777215, 24));
        textEditColor->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        textEditColor->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        horizontalLayoutShapeColor->addWidget(textEditColor);


        verticalLayout->addLayout(horizontalLayoutShapeColor);

        buttonBoxValidation = new QDialogButtonBox(CreateNodeDialog);
        buttonBoxValidation->setObjectName(QString::fromUtf8("buttonBoxValidation"));
        buttonBoxValidation->setOrientation(Qt::Horizontal);
        buttonBoxValidation->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        buttonBoxValidation->setCenterButtons(true);

        verticalLayout->addWidget(buttonBoxValidation);


        retranslateUi(CreateNodeDialog);
        QObject::connect(buttonBoxValidation, SIGNAL(accepted()), CreateNodeDialog, SLOT(accept()));
        QObject::connect(buttonBoxValidation, SIGNAL(rejected()), CreateNodeDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(CreateNodeDialog);
    } // setupUi

    void retranslateUi(QDialog *CreateNodeDialog)
    {
        CreateNodeDialog->setWindowTitle(QApplication::translate("CreateNodeDialog", "Create a Node", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        CreateNodeDialog->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        labelName->setText(QApplication::translate("CreateNodeDialog", "Nom", 0, QApplication::UnicodeUTF8));
        textEditName->setDocumentTitle(QString());
        textEditName->setHtml(QApplication::translate("CreateNodeDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", 0, QApplication::UnicodeUTF8));
        labelPos->setText(QApplication::translate("CreateNodeDialog", "Position", 0, QApplication::UnicodeUTF8));
        labelX->setText(QApplication::translate("CreateNodeDialog", "X", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_ACCESSIBILITY
        spinBoxX->setAccessibleName(QApplication::translate("CreateNodeDialog", "nodeX", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_ACCESSIBILITY
        labelY->setText(QApplication::translate("CreateNodeDialog", "Y", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_ACCESSIBILITY
        spinBoxY->setAccessibleName(QApplication::translate("CreateNodeDialog", "nodeY", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_ACCESSIBILITY
        labelSize->setText(QApplication::translate("CreateNodeDialog", "Taille", 0, QApplication::UnicodeUTF8));
        labelShape->setText(QApplication::translate("CreateNodeDialog", "Forme", 0, QApplication::UnicodeUTF8));
        labelColor->setText(QApplication::translate("CreateNodeDialog", "Couleur", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CreateNodeDialog: public Ui_CreateNodeDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CREATENODEDIALOG_H
