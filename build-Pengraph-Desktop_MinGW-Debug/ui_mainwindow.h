/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "GLGraphView.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionNouveau;
    QAction *actionOuvrir;
    QAction *actionEnregistrer;
    QAction *actionEnregistrer_sous;
    QAction *actionSupprimer;
    QAction *actionQuitter;
    QAction *actionSommets;
    QAction *actionAr_tes;
    QAction *actionAr_tes_2;
    QAction *actionSommets_2;
    QAction *actionAr_tes_3;
    QAction *actionEtiquettes;
    QAction *actionCouper;
    QAction *actionCopier;
    QAction *actionColler;
    QAction *actionAnnuler;
    QAction *actionRefaire;
    QAction *actionD_zoom;
    QAction *actionVers_l_avant;
    QAction *actionVers_l_arri_re;
    QAction *actionVers_l_avant_2;
    QAction *actionVers_l_arri_re_2;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QVBoxLayout *mainLayout;
    GLGraphView *glGraphView;
    QMenuBar *menuBar;
    QMenu *menuFichier;
    QMenu *menuEditer;
    QMenu *menuS_lection;
    QMenu *menuAffichage;
    QMenu *menuZoom_2;
    QMenu *menuInformations;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 800);
        MainWindow->setBaseSize(QSize(1024, 768));
        actionNouveau = new QAction(MainWindow);
        actionNouveau->setObjectName(QString::fromUtf8("actionNouveau"));
        QIcon icon;
        QString iconThemeName = QString::fromUtf8("document-new");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionNouveau->setIcon(icon);
        actionOuvrir = new QAction(MainWindow);
        actionOuvrir->setObjectName(QString::fromUtf8("actionOuvrir"));
        QIcon icon1;
        iconThemeName = QString::fromUtf8("document-open");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon1 = QIcon::fromTheme(iconThemeName);
        } else {
            icon1.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionOuvrir->setIcon(icon1);
        actionEnregistrer = new QAction(MainWindow);
        actionEnregistrer->setObjectName(QString::fromUtf8("actionEnregistrer"));
        actionEnregistrer_sous = new QAction(MainWindow);
        actionEnregistrer_sous->setObjectName(QString::fromUtf8("actionEnregistrer_sous"));
        QIcon icon2;
        iconThemeName = QString::fromUtf8("document-save-as");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon2 = QIcon::fromTheme(iconThemeName);
        } else {
            icon2.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionEnregistrer_sous->setIcon(icon2);
        actionSupprimer = new QAction(MainWindow);
        actionSupprimer->setObjectName(QString::fromUtf8("actionSupprimer"));
        actionQuitter = new QAction(MainWindow);
        actionQuitter->setObjectName(QString::fromUtf8("actionQuitter"));
        QIcon icon3;
        iconThemeName = QString::fromUtf8("document-close");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon3 = QIcon::fromTheme(iconThemeName);
        } else {
            icon3.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionQuitter->setIcon(icon3);
        actionSommets = new QAction(MainWindow);
        actionSommets->setObjectName(QString::fromUtf8("actionSommets"));
        QIcon icon4;
        iconThemeName = QString::fromUtf8("draw-path");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon4 = QIcon::fromTheme(iconThemeName);
        } else {
            icon4.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionSommets->setIcon(icon4);
        actionAr_tes = new QAction(MainWindow);
        actionAr_tes->setObjectName(QString::fromUtf8("actionAr_tes"));
        QIcon icon5;
        iconThemeName = QString::fromUtf8("edit-node");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon5 = QIcon::fromTheme(iconThemeName);
        } else {
            icon5.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionAr_tes->setIcon(icon5);
        actionAr_tes_2 = new QAction(MainWindow);
        actionAr_tes_2->setObjectName(QString::fromUtf8("actionAr_tes_2"));
        QIcon icon6;
        iconThemeName = QString::fromUtf8("draw-line");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon6 = QIcon::fromTheme(iconThemeName);
        } else {
            icon6.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionAr_tes_2->setIcon(icon6);
        actionSommets_2 = new QAction(MainWindow);
        actionSommets_2->setObjectName(QString::fromUtf8("actionSommets_2"));
        actionSommets_2->setIcon(icon5);
        actionAr_tes_3 = new QAction(MainWindow);
        actionAr_tes_3->setObjectName(QString::fromUtf8("actionAr_tes_3"));
        actionAr_tes_3->setIcon(icon6);
        actionEtiquettes = new QAction(MainWindow);
        actionEtiquettes->setObjectName(QString::fromUtf8("actionEtiquettes"));
        QIcon icon7;
        iconThemeName = QString::fromUtf8("edit-guides");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon7 = QIcon::fromTheme(iconThemeName);
        } else {
            icon7.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionEtiquettes->setIcon(icon7);
        actionCouper = new QAction(MainWindow);
        actionCouper->setObjectName(QString::fromUtf8("actionCouper"));
        QIcon icon8;
        iconThemeName = QString::fromUtf8("edit-cut");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon8 = QIcon::fromTheme(iconThemeName);
        } else {
            icon8.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionCouper->setIcon(icon8);
        actionCopier = new QAction(MainWindow);
        actionCopier->setObjectName(QString::fromUtf8("actionCopier"));
        QIcon icon9;
        iconThemeName = QString::fromUtf8("edit-copy");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon9 = QIcon::fromTheme(iconThemeName);
        } else {
            icon9.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionCopier->setIcon(icon9);
        actionColler = new QAction(MainWindow);
        actionColler->setObjectName(QString::fromUtf8("actionColler"));
        QIcon icon10;
        iconThemeName = QString::fromUtf8("edit-paste");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon10 = QIcon::fromTheme(iconThemeName);
        } else {
            icon10.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionColler->setIcon(icon10);
        actionAnnuler = new QAction(MainWindow);
        actionAnnuler->setObjectName(QString::fromUtf8("actionAnnuler"));
        QIcon icon11;
        iconThemeName = QString::fromUtf8("edit-undo");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon11 = QIcon::fromTheme(iconThemeName);
        } else {
            icon11.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionAnnuler->setIcon(icon11);
        actionRefaire = new QAction(MainWindow);
        actionRefaire->setObjectName(QString::fromUtf8("actionRefaire"));
        QIcon icon12;
        iconThemeName = QString::fromUtf8("edit-redo");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon12 = QIcon::fromTheme(iconThemeName);
        } else {
            icon12.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionRefaire->setIcon(icon12);
        actionD_zoom = new QAction(MainWindow);
        actionD_zoom->setObjectName(QString::fromUtf8("actionD_zoom"));
        actionVers_l_avant = new QAction(MainWindow);
        actionVers_l_avant->setObjectName(QString::fromUtf8("actionVers_l_avant"));
        actionVers_l_arri_re = new QAction(MainWindow);
        actionVers_l_arri_re->setObjectName(QString::fromUtf8("actionVers_l_arri_re"));
        actionVers_l_avant_2 = new QAction(MainWindow);
        actionVers_l_avant_2->setObjectName(QString::fromUtf8("actionVers_l_avant_2"));
        actionVers_l_arri_re_2 = new QAction(MainWindow);
        actionVers_l_arri_re_2->setObjectName(QString::fromUtf8("actionVers_l_arri_re_2"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        mainLayout = new QVBoxLayout();
        mainLayout->setSpacing(6);
        mainLayout->setObjectName(QString::fromUtf8("mainLayout"));
        glGraphView = new GLGraphView(centralWidget);
        glGraphView->setObjectName(QString::fromUtf8("glGraphView"));

        mainLayout->addWidget(glGraphView);


        gridLayout->addLayout(mainLayout, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 800, 29));
        menuFichier = new QMenu(menuBar);
        menuFichier->setObjectName(QString::fromUtf8("menuFichier"));
        menuEditer = new QMenu(menuBar);
        menuEditer->setObjectName(QString::fromUtf8("menuEditer"));
        menuS_lection = new QMenu(menuBar);
        menuS_lection->setObjectName(QString::fromUtf8("menuS_lection"));
        menuAffichage = new QMenu(menuBar);
        menuAffichage->setObjectName(QString::fromUtf8("menuAffichage"));
        menuZoom_2 = new QMenu(menuAffichage);
        menuZoom_2->setObjectName(QString::fromUtf8("menuZoom_2"));
        QIcon icon13;
        iconThemeName = QString::fromUtf8("edit-find");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon13 = QIcon::fromTheme(iconThemeName);
        } else {
            icon13.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        menuZoom_2->setIcon(icon13);
        menuInformations = new QMenu(menuBar);
        menuInformations->setObjectName(QString::fromUtf8("menuInformations"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFichier->menuAction());
        menuBar->addAction(menuEditer->menuAction());
        menuBar->addAction(menuAffichage->menuAction());
        menuBar->addAction(menuS_lection->menuAction());
        menuBar->addAction(menuInformations->menuAction());
        menuFichier->addAction(actionNouveau);
        menuFichier->addAction(actionOuvrir);
        menuFichier->addSeparator();
        menuFichier->addAction(actionEnregistrer_sous);
        menuFichier->addSeparator();
        menuFichier->addAction(actionQuitter);
        menuEditer->addAction(actionAnnuler);
        menuEditer->addAction(actionRefaire);
        menuEditer->addSeparator();
        menuEditer->addAction(actionCouper);
        menuEditer->addAction(actionCopier);
        menuEditer->addAction(actionColler);
        menuEditer->addSeparator();
        menuS_lection->addAction(actionSommets);
        menuS_lection->addAction(actionAr_tes);
        menuS_lection->addAction(actionAr_tes_2);
        menuAffichage->addAction(actionSommets_2);
        menuAffichage->addAction(actionAr_tes_3);
        menuAffichage->addAction(actionEtiquettes);
        menuAffichage->addSeparator();
        menuAffichage->addAction(menuZoom_2->menuAction());
        menuZoom_2->addAction(actionVers_l_avant_2);
        menuZoom_2->addAction(actionVers_l_arri_re_2);

        retranslateUi(MainWindow);
        QObject::connect(actionNouveau, SIGNAL(triggered()), MainWindow, SLOT(newGraph()));
        QObject::connect(actionQuitter, SIGNAL(triggered()), MainWindow, SLOT(close()));
        QObject::connect(actionOuvrir, SIGNAL(triggered()), MainWindow, SLOT(openGraph()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Pengraph", 0, QApplication::UnicodeUTF8));
        actionNouveau->setText(QApplication::translate("MainWindow", "Nouveau", 0, QApplication::UnicodeUTF8));
        actionNouveau->setShortcut(QApplication::translate("MainWindow", "Ctrl+N", 0, QApplication::UnicodeUTF8));
        actionOuvrir->setText(QApplication::translate("MainWindow", "Ouvrir...", 0, QApplication::UnicodeUTF8));
        actionOuvrir->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", 0, QApplication::UnicodeUTF8));
        actionEnregistrer->setText(QApplication::translate("MainWindow", "Enregistrer", 0, QApplication::UnicodeUTF8));
        actionEnregistrer_sous->setText(QApplication::translate("MainWindow", "Enregistrer sous...", 0, QApplication::UnicodeUTF8));
        actionEnregistrer_sous->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", 0, QApplication::UnicodeUTF8));
        actionSupprimer->setText(QApplication::translate("MainWindow", "Supprimer", 0, QApplication::UnicodeUTF8));
        actionQuitter->setText(QApplication::translate("MainWindow", "Quitter", 0, QApplication::UnicodeUTF8));
        actionQuitter->setShortcut(QApplication::translate("MainWindow", "Alt+Esc", 0, QApplication::UnicodeUTF8));
        actionSommets->setText(QApplication::translate("MainWindow", "Tout", 0, QApplication::UnicodeUTF8));
        actionSommets->setShortcut(QApplication::translate("MainWindow", "Ctrl+A", 0, QApplication::UnicodeUTF8));
        actionAr_tes->setText(QApplication::translate("MainWindow", "Sommets", 0, QApplication::UnicodeUTF8));
        actionAr_tes->setShortcut(QApplication::translate("MainWindow", "Ctrl+Shift+N", 0, QApplication::UnicodeUTF8));
        actionAr_tes_2->setText(QApplication::translate("MainWindow", "Ar\303\252tes", 0, QApplication::UnicodeUTF8));
        actionAr_tes_2->setShortcut(QApplication::translate("MainWindow", "Ctrl+Shift+E", 0, QApplication::UnicodeUTF8));
        actionSommets_2->setText(QApplication::translate("MainWindow", "Sommets", 0, QApplication::UnicodeUTF8));
        actionSommets_2->setShortcut(QApplication::translate("MainWindow", "Ctrl+Alt+N", 0, QApplication::UnicodeUTF8));
        actionAr_tes_3->setText(QApplication::translate("MainWindow", "Ar\303\252tes", 0, QApplication::UnicodeUTF8));
        actionAr_tes_3->setShortcut(QApplication::translate("MainWindow", "Ctrl+Alt+E", 0, QApplication::UnicodeUTF8));
        actionEtiquettes->setText(QApplication::translate("MainWindow", "Etiquettes", 0, QApplication::UnicodeUTF8));
        actionEtiquettes->setShortcut(QApplication::translate("MainWindow", "Ctrl+Alt+L", 0, QApplication::UnicodeUTF8));
        actionCouper->setText(QApplication::translate("MainWindow", "Couper", 0, QApplication::UnicodeUTF8));
        actionCouper->setShortcut(QApplication::translate("MainWindow", "Ctrl+X", 0, QApplication::UnicodeUTF8));
        actionCopier->setText(QApplication::translate("MainWindow", "Copier", 0, QApplication::UnicodeUTF8));
        actionCopier->setShortcut(QApplication::translate("MainWindow", "Ctrl+C", 0, QApplication::UnicodeUTF8));
        actionColler->setText(QApplication::translate("MainWindow", "Coller", 0, QApplication::UnicodeUTF8));
        actionColler->setShortcut(QApplication::translate("MainWindow", "Ctrl+V", 0, QApplication::UnicodeUTF8));
        actionAnnuler->setText(QApplication::translate("MainWindow", "Annuler", 0, QApplication::UnicodeUTF8));
        actionAnnuler->setShortcut(QApplication::translate("MainWindow", "Ctrl+Z", 0, QApplication::UnicodeUTF8));
        actionRefaire->setText(QApplication::translate("MainWindow", "Refaire", 0, QApplication::UnicodeUTF8));
        actionRefaire->setShortcut(QApplication::translate("MainWindow", "Ctrl+Y", 0, QApplication::UnicodeUTF8));
        actionD_zoom->setText(QApplication::translate("MainWindow", "D\303\251zoom", 0, QApplication::UnicodeUTF8));
        actionVers_l_avant->setText(QApplication::translate("MainWindow", "Vers l'avant", 0, QApplication::UnicodeUTF8));
        actionVers_l_arri_re->setText(QApplication::translate("MainWindow", "Vers l'arri\303\250re", 0, QApplication::UnicodeUTF8));
        actionVers_l_avant_2->setText(QApplication::translate("MainWindow", "Vers l'avant", 0, QApplication::UnicodeUTF8));
        actionVers_l_avant_2->setShortcut(QApplication::translate("MainWindow", "+", 0, QApplication::UnicodeUTF8));
        actionVers_l_arri_re_2->setText(QApplication::translate("MainWindow", "Vers l'arri\303\250re", 0, QApplication::UnicodeUTF8));
        actionVers_l_arri_re_2->setShortcut(QApplication::translate("MainWindow", "-", 0, QApplication::UnicodeUTF8));
        menuFichier->setTitle(QApplication::translate("MainWindow", "Fichier", 0, QApplication::UnicodeUTF8));
        menuEditer->setTitle(QApplication::translate("MainWindow", "\303\211dition", 0, QApplication::UnicodeUTF8));
        menuS_lection->setTitle(QApplication::translate("MainWindow", "S\303\251lection", 0, QApplication::UnicodeUTF8));
        menuAffichage->setTitle(QApplication::translate("MainWindow", "Affichage", 0, QApplication::UnicodeUTF8));
        menuZoom_2->setTitle(QApplication::translate("MainWindow", "Zoom", 0, QApplication::UnicodeUTF8));
        menuInformations->setTitle(QApplication::translate("MainWindow", "\303\200 propos...", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
